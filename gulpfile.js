var $ = require('gulp-load-plugins')({ lazy: true });
var gulp = require("gulp");
var ts = require('gulp-typescript');
var jasmine = require('gulp-jasmine');
var reporters = require('jasmine-reporters');
var gutil = require('gulp-util');
var cfenv = require('cfenv');
var tslint = require('tslint');
const testReporter = require("jasmine2-reporter");

var tsProject = ts.createProject('tsconfig.json');

gulp.task('compile', ['lint'], function () {
  var tsResult = tsProject.src()
    .pipe(tsProject())
    .once("error", function () {
      this.once("finish", () => process.exit(1));
    });

  return tsResult.js.pipe(gulp.dest('./src'));
});

gulp.task('lint', () => {
  var program = tslint.Linter.createProgram("./tsconfig.json");

  return gulp.src('./src/**/*.ts')
    .pipe($.tslint({
      program: program,
      formattersDirectory: 'node_modules/custom-tslint-formatters/formatters',
      formatter: 'grouped'
    }))
    .pipe($.tslint.report({
      emitError: true,
      summarizeFailureOutput: true
    }));
});

gulp.task('jasmine', ['compile'], () =>
  gulp.src('./src/tests/**/*.spec.js')
    .pipe(jasmine({
      reporter: new testReporter.Jasmine2Reporter({
        stacktrace: false,
        failuresSummary: false,
      }),
    }))
);

gulp.task('default', ['jasmine']);
gulp.task('test', ['jasmine']);

