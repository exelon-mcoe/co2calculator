// Include dependencies
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

import { HelloWorld, CO2Calc  } from './controllers';
import { ApiErrorHandler } from './services';

export interface AppServices {
  app?: express.Express;
  errorHandler?: ApiErrorHandler;
}

// Main app
export class App {
  private _app: express.Express;
  private _errorHandler: ApiErrorHandler;

  constructor(services: AppServices = {}) {
    this._app = services.app || express();

    this._errorHandler = services.errorHandler || new ApiErrorHandler();

    this._registerMiddleware();
    this._registerRoutes();
    this._registerErrorHandlers();
  }

  get express(): express.Express {
    return this._app;
  }

  private _registerMiddleware() {
    this._app.use(logger('dev'));
    this._app.use(bodyParser.json({ type: 'application/json' }));
    this._app.use(bodyParser.urlencoded({ extended: false }));
  }

  private _registerRoutes() {
    this._app.get('/',
      (req: express.Request, res: express.Response, next: express.NextFunction) =>
        HelloWorld.healthCheck(req, res));
    this._app.post('/accelEmissions',
      (req: express.Request, res: express.Response, next: express.NextFunction) =>
        CO2Calc.calculateAccelEmissions(req, res));
    this._app.post('/cruiseEmissions',
      (req: express.Request, res: express.Response, next: express.NextFunction) =>
        CO2Calc.calculateCruiseEmissions(req, res));
    this._app.post('/idleEmissions',
      (req: express.Request, res: express.Response, next: express.NextFunction) =>
        CO2Calc.calculateIdleEmissions(req, res));
  }

  private _registerErrorHandlers() {
    // catch 404 and forward to error handler
    this._app.use((req: express.Request, res: express.Response, next: Function) => {
      let err = new Error('Not Found');
      res.status(404);
      console.log('catching 404 error');
      return next(err);
    });

    this._app.use(this._errorHandler.createErrorHandler());
  }

}
