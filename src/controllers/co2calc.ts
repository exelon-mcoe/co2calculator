import { Request, Response } from 'express';
import { AccelerationCO2Request, CrusingCO2Request, IdleCO2Request } from '../interfaces';
import { CalculatorService } from '../services';

export class CO2Calc {

  public static calculateIdleEmissions(req: Request, res: Response) {
    let requestData: IdleCO2Request = req.body;
    let svc = new CalculatorService();
    let result = svc.getIdleCarbon(requestData.vehicleMpg, requestData.idleTime, 19.1);
    res.send(result);
    res.end();
  }

  public static calculateCruiseEmissions(req: Request, res: Response) {
    let requestData: CrusingCO2Request = req.body;
    let svc = new CalculatorService();
    let result = svc.getCruisingCarbon(requestData.vehicleMpg, requestData.speedLimit, requestData.duration, 19.1);
    res.send(result);
    res.end();
  }

  public static calculateAccelEmissions(req: Request, res: Response) {
    let requestData: AccelerationCO2Request = req.body;
    let svc = new CalculatorService();
    let accelRate = requestData.accelerationRate || 5;
    let result = svc.getAccellerationCarbon(requestData.vehicleMpg, requestData.speedLimit, accelRate, 19.1);
    res.send(result);
    res.end();
  }
}
