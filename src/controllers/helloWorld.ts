import { Request, Response } from 'express';
import * as marked from 'marked';

export class HelloWorld {

  public static healthCheck(req: Request, res: Response) {
    let data = '# Carbon Calculator' + '\n';
    data += '## Acceleration Emissions' + '\n';
    data += '`/accelEmissions`' + '\n';
    data += '' + '\n';
    data += 'Request Data:' + '\n';
    data += '```' + '\n';
    data += 'POST:' + '\n';
    data += '{' + '\n';
    data += '  vehicleMpg: number;' + '\n';
    data += '  speedLimit: number;' + '\n';
    data += '  accelerationRate?: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    data += 'Response Data:' + '\n';
    data += '```' + '\n';
    data += '{' + '\n';
    data += '  gallons: number;' + '\n';
    data += '  emissions: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    data += '' + '\n';
    data += '## Cruising Emissions' + '\n';
    data += '      `/cruiseEmissions`' + '\n';
    data += 'Request Data:' + '\n';
    data += '```' + '\n';
    data += 'POST:' + '\n';
    data += '{' + '\n';
    data += '  vehicleMpg: number;' + '\n';
    data += '  speedLimit: number;' + '\n';
    data += '  duration?: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    data += 'Response Data:' + '\n';
    data += '```' + '\n';
    data += '{' + '\n';
    data += '  gallons: number;' + '\n';
    data += '  emissions: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    data += '## Idle Emissions' + '\n';
    data += '`/idleEmissions`' + '\n';
    data += '    Request Data:' + '\n';
    data += '    ```' + '\n';
    data += 'POST:' + '\n';
    data += '{' + '\n';
    data += '  vehicleMpg: number;' + '\n';
    data += '  idleTime: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    data += 'Response Data:' + '\n';
    data += '```' + '\n';
    data += '{' + '\n';
    data += '  gallons: number;' + '\n';
    data += '  emissions: number;' + '\n';
    data += '}' + '\n';
    data += '```' + '\n';
    res.send(marked(data));
  }
}
