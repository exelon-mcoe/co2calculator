import { CalculatorService } from '../services';

describe('CalculatorService', () => {
  it('should getAccellerationCarbon', () => {
    let svc = new CalculatorService();
    let result = svc.getAccellerationCarbon(21.2, 35, 5, 19.1);
    console.log(result);
    expect(result).toBeDefined();
  });

  it('should getIdleCarbon', () => {
    let svc = new CalculatorService();
    let result = svc.getIdleCarbon(22, 30, 19.1);
    let resultHeavy = svc.getIdleCarbon(12, 30, 19.1);
    let resultLight = svc.getIdleCarbon(22, 60, 19.1);
    console.log(result);
    console.log(resultHeavy);
    console.log(resultLight);
    expect(result).toBeDefined();

  });

  it('should getCruisingCarbon', () => {
    let svc = new CalculatorService();
    let result = svc.getCruisingCarbon(22, 35, 30, 19.1);
    console.log(result);
    expect(result).toBeDefined();
  });
});
