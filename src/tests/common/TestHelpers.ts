import * as chalk from 'chalk';

export class TestHelpers {
  static createDoneFn(self: any, jasmineDoneFn: any) {
    // The jasmine DoneFn requires that the function use `.bind(self)`

    let myDoneFn = (err: any, res: any) => {
      if (err) {
        // Enable logging as needed for debugging
        if (res) {
          console.log(chalk.blue('Response status:'), res.status);
          console.log(chalk.blue('Response headers:'), res.headers);
          console.log(chalk.blue('Response body:'), res.body);
          console.log(chalk.blue('Response error:'), res.error);

          // console.log(chalk.blue('ERROR:'), err);
          // Error logging is not needed here if tslint is configured with
          //   .pipe($.tslint.report({
          //     emitError: true,
          //   }));
        }

        jasmineDoneFn.fail(err);
        return;
      }

      jasmineDoneFn();
    };

    return myDoneFn.bind(self);
  }
}
