// import * as express from 'express';
import * as request from 'supertest';
import { App } from '../app';
import { TestHelpers } from './common';

describe('App', () => {
  /* tslint:disable:no-invalid-this */
  // disabled for:  .end(TestHelpers.createDoneFn(this, done))
  // rule is not aware of jasmine's use of 'this'

  let _app: App;

  beforeEach(() => {
    _app = new App();
    // do not start the server!
    // supertest will start/stop it as needed
  });

  it('should say hello', done => {
    request(_app.express)
      .get('/')
      .expect(200)
      .end(TestHelpers.createDoneFn(this, done));
  });

});
