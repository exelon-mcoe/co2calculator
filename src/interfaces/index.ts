
export interface Config {
  something: string;
}

export interface IdleCO2Request {
  vehicleMpg: number;
  idleTime: number;
}

export interface CrusingCO2Request {
  vehicleMpg: number;
  speedLimit: number;
  duration?: number;
}

export interface AccelerationCO2Request {
  vehicleMpg: number;
  speedLimit: number;
  accelerationRate?: number;
}

export interface CO2Request {
  vehiclecount: number;
  speedlimit: number;
  idleduration: number;
}

export interface CO2Response {
  gallons: number;
  emissions: number;
}
