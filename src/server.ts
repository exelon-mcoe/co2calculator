// Boot server
import * as debug from 'debug';
import { App } from './app';

const port = process.env.PORT || 3000;

let app = new App();
app.express.set('port', port);

app.express.listen(app.express.get('port'), () => {
  debug('Express server listening on port ' + port);
}).on('error', err => {
  console.log('Cannot start server, port most likely in use');
  console.log(err);
});
