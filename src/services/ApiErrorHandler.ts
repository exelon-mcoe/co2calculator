import { Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import * as statuses from 'statuses';

// Based on https://github.com/expressjs/api-error-handler
// Will honor errors from https://github.com/jshttp/http-errors
// And err.errors supplied by the DataValidationService

export class ApiErrorHandler {
  production = process.env.NODE_ENV === 'production';
  verbose = true;

  createErrorHandler(): ErrorRequestHandler {
    return (err: any, req: Request, res: Response, next: NextFunction) => {
      this._errorHandler(err, req, res, next);
    };
  }

  _errorHandler(err: any, req: Request, res: Response, next: NextFunction) {
    if (this.verbose) {
      console.error('[ApiErrorHandler]', err);
    }

    let status = err.status || err.statusCode || 500;
    if (status < 100) { status = 500; }
    res.statusCode = status;

    let body: any = {
      status: status,
    };

    // show the stacktrace when not in production
    if (!this.production) { body.stack = err.stack; }

    // internal server errors
    let expose = err.expose || status < 500;
    if (!expose) {
      body.message = statuses[status];
      res.json(body);
      return;
    }

    // client errors
    body.message = err.message;

    if (err.code) { body.code = err.code; }
    if (err.name) { body.name = err.name; }
    if (err.type) { body.type = err.type; }
    if (err.errors) { body.errors = err.errors; }

    res.json(body);
  }

}
