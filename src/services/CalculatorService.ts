import { CO2Response } from '../interfaces';

export class CalculatorService {

  getIdleCarbon(vehicleMpg: number, idleTime: number, emissionsFactor: number): CO2Response {
    let consumptionRate: number = 20 / vehicleMpg * 0.3 / 3600;
    let result: CO2Response = {
      gallons: idleTime * consumptionRate,
      emissions: idleTime * consumptionRate * emissionsFactor,
    };

    return result;
  }

  getCruisingCarbon(vehicleMpg: number, speedLimit: number, duration: number, emissionsFactor: number): CO2Response {
    let result: CO2Response = {
      gallons: (vehicleMpg / 3600) * (speedLimit / 3600) * duration,
      emissions: (vehicleMpg / 3600) * (speedLimit / 3600) * duration * emissionsFactor,
    };

    return result;
  }

  getAccellerationCarbon(vehicleMpg: number, speedLimit: number, accellerationRate: number, emissionsFactor: number): CO2Response {
    let curSpeed = 0;
    let totalDistance = 0;
    let fuelConsumed = 0;
    speedLimit = speedLimit / 3600;
    accellerationRate = accellerationRate / 3600;


    while ( curSpeed <= speedLimit ) {
      let distance = curSpeed + (accellerationRate / 2);
      totalDistance += distance;
      let curMpg = vehicleMpg * ((curSpeed + (accellerationRate / 2)) / speedLimit);
      console.log(`curSpeed: ${curSpeed}; curMpg: ${curMpg}; distance: ${distance}; totalDistance: ${totalDistance}`);
      fuelConsumed += distance / curMpg;
      console.log(`fuelConsumed: ${fuelConsumed}; fuelConsumed_interval: ${distance / curMpg}`);
      curSpeed += accellerationRate;

    }
    let result: CO2Response = {
      gallons: fuelConsumed,
      emissions: fuelConsumed * emissionsFactor,
    };

    return result;
  }

}
