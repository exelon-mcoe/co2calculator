# Carbon Calculator
## Acceleration Emissions
`/accelEmissions`

Request Data:
```
POST:
{
  vehicleMpg: number;
  speedLimit: number;
  accelerationRate?: number;
}
```
Response Data:
```
{
  gallons: number;
  emissions: number;
}
```

## Cruising Emissions
`/cruiseEmissions`

Request Data:
```
POST:
{
  vehicleMpg: number;
  speedLimit: number;
  duration?: number;
}
```
Response Data:
```
{
  gallons: number;
  emissions: number;
}
```

## Idle Emissions
`/idleEmissions`
Request Data:
```
POST:
{
  vehicleMpg: number;
  idleTime: number;
}
```
Response Data:
```
{
  gallons: number;
  emissions: number;
}
```
